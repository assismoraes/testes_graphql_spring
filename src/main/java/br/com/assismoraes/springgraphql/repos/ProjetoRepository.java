package br.com.assismoraes.springgraphql.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.assismoraes.springgraphql.models.Projeto;

@Repository
public interface ProjetoRepository extends JpaRepository<Projeto, Long> {

}