package br.com.assismoraes.springgraphql.resolvers;

import java.util.List;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;

import br.com.assismoraes.springgraphql.models.Empregado;
import br.com.assismoraes.springgraphql.repos.EmpregadoRepository;
import br.com.assismoraes.springgraphql.repos.ProjetoRepository;

public class Query implements GraphQLQueryResolver {

	   private EmpregadoRepository empregadoRepository;
	   private ProjetoRepository projetoRepository;

	   public Query(EmpregadoRepository empregadoRepository, ProjetoRepository projetoRepository) {
	      this.empregadoRepository = empregadoRepository;
	      this.projetoRepository = projetoRepository;
	   }

	   public List<Empregado> obterEmpregados() {
	      return empregadoRepository.findAll();
	   }

	   public long contarEmpregados() {
	      return empregadoRepository.count();
	   }

	   public Empregado obterEmpregadoPorId(Long id) {
	      return empregadoRepository.findOne(id);
	   }

	}
