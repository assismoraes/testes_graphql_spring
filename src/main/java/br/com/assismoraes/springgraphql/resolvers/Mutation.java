package br.com.assismoraes.springgraphql.resolvers;

import java.util.Date;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;

import br.com.assismoraes.springgraphql.models.Empregado;
import br.com.assismoraes.springgraphql.repos.EmpregadoRepository;
import br.com.assismoraes.springgraphql.repos.ProjetoRepository;


public class Mutation implements GraphQLMutationResolver {

	   private EmpregadoRepository empregadoRepository;
	   private ProjetoRepository projetoRepository;

	   public Mutation(EmpregadoRepository empregadoRepository, ProjetoRepository projetoRepository) {
	      this.empregadoRepository = empregadoRepository;
	      this.projetoRepository = projetoRepository;
	   }

	   public Empregado novoEmpregado(String nome, Integer idade) {
	      Empregado empregado = new Empregado(nome, idade, new Date());
	      empregadoRepository.save(empregado);
	      return empregado;
	   }

	   public boolean deletarEmpregado(Long id) {
	      empregadoRepository.delete(id);
	      return true;
	   }
}