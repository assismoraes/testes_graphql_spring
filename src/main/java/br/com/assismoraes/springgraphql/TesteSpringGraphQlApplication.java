package br.com.assismoraes.springgraphql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.assismoraes.springgraphql.repos.EmpregadoRepository;
import br.com.assismoraes.springgraphql.repos.ProjetoRepository;
import br.com.assismoraes.springgraphql.resolvers.Mutation;
import br.com.assismoraes.springgraphql.resolvers.Query;

@SpringBootApplication
public class TesteSpringGraphQlApplication {

	public static void main(String[] args) {
		SpringApplication.run(TesteSpringGraphQlApplication.class, args);
	}
	
	@Bean
	public Query query(EmpregadoRepository empregadoRepository, ProjetoRepository projetoRepository) {
		return new Query(empregadoRepository, projetoRepository);
	}

	@Bean
	public Mutation mutation(EmpregadoRepository empregadoRepository, ProjetoRepository projetoRepository) {
		return new Mutation(empregadoRepository, projetoRepository);
	}

}
